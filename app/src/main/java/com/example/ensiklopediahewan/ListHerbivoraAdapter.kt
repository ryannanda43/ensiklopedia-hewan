package com.example.ensiklopediahewan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_row_herbivora.view.*

class ListHerbivoraAdapter(private val listHerbivora: ArrayList<Herbivora>) : RecyclerView.Adapter<ListHerbivoraAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_row_herbivora, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(listHerbivora[position])
    }

    override fun getItemCount(): Int = listHerbivora.size

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(herbivora: Herbivora) {
            with(itemView){
                Glide.with(itemView.context)
                    .load(herbivora.photo)
                    .apply(RequestOptions().override(55, 55))
                    .into(img_item_photo)
                tv_item_name.text = herbivora.name
                tv_item_description.text = herbivora.description
            }
        }
    }
}