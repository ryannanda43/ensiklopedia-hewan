package com.example.ensiklopediahewan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_row_karnivora.view.*

class ListKarnivoraAdapter(private val listKarnivora: ArrayList<Karnivora>) : RecyclerView.Adapter<ListKarnivoraAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_row_karnivora, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(listKarnivora[position])
    }

    override fun getItemCount(): Int = listKarnivora.size

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(karnivora: Karnivora) {
            with(itemView){
                Glide.with(itemView.context)
                    .load(karnivora.photo)
                    .apply(RequestOptions().override(55, 55))
                    .into(img_item_photo)
                tv_item_name.text = karnivora.name
                tv_item_description.text = karnivora.description
            }
        }
    }
}